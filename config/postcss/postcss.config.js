module.exports = {
  plugins: [
    require('autoprefixer'),
    require('cssnano')({
      preset: [
        'postcss-preset-env',
        {
          discardComments: {
            removeAll: true
          }
        }
      ]
    })
  ]
};
