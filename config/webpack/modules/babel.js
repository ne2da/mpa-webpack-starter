module.exports = () => {
  return {
    module: {
      rules: [
        {
          test: /\.(js|jsx|mjs)$/,
          exclude: /node_modules/,
          use: [
            {
              loader: 'babel-loader',
              options: {
                presets: ['@babel/preset-env'],

                plugins: [
                  '@babel/plugin-transform-runtime',
                  '@babel/plugin-proposal-class-properties'
                  // '@babel/plugin-syntax-async-generators',
                  // '@babel/plugin-syntax-dynamic-import'
                ]
              }
            }
          ]
        }
      ]
    }
  };
};
