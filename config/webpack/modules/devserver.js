module.exports = function () {
  return {
    devServer: {
      // contentBase: path.resolve(__dirname, 'build'),
      // watchContentBase: true,
      port: 8084,
      disableHostCheck: true,
      stats: 'normal',
      hot: true,
      compress: true,
      overlay: true,
      open: true,
      historyApiFallback: true,
      watchOptions: {
        aggregateTimeout: 100,
        poll: true,
        ignored: 'node_modules'
      }
    }
  };
};
