const IMG_FILENAME_DEV = require('../utils/constants.js').FILENAMES
  .FILENAME_DEV;

module.exports = () => {
  return {
    module: {
      rules: [
        {
          test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
          loader: 'file-loader',
          options: {
            name: `fonts/${IMG_FILENAME_DEV}`
          }
        }
      ]
    }
  };
};
