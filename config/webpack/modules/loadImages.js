const IMG_FILENAME_DEV = require('../utils/constants.js').FILENAMES
  .FILENAME_DEV;

module.exports = () => {
  return {
    module: {
      rules: [
        {
          test: /\.(jpg|jpeg|png|gif|svg)$/,
          loader: 'file-loader',
          options: {
            name: `images/${IMG_FILENAME_DEV}`
          }
        }
      ]
    }
  };
};
