const ImageminPlugin = require('imagemin-webpack');
const IMG_FILENAME_PROD = require('../utils/constants.js').FILENAMES
  .FILENAME_PROD;

module.exports = () => {
  return {
    module: {
      rules: [
        {
          test: /\.(jpg|jpeg|png)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: `images/${IMG_FILENAME_PROD}`
              }
            },
            {
              loader: 'webp-loader',
              options: {
                quality: 13
              }
            }
          ]
        },
        {
          test: /\.(gif|svg)$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: `images/${IMG_FILENAME_PROD}`
              }
            },
            {
              loader: ImageminPlugin.loader,
              options: {
                bail: false,
                cache: false,
                imageminOptions: {
                  plugins: [
                    ['gifsicle', { interlaced: true, optimizationLevel: 3 }],
                    ['svgo', { plugins: [{ removeViewBox: false }] }]
                  ]
                }
              }
            }
          ]
        }
      ]
    }
  };
};
