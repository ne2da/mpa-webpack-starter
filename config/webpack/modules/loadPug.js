const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

// Tools | Helpers
const { PUG_PAGE_FILES } = require('../utils/constants');

module.exports = () => {
  return {
    module: {
      rules: [
        {
          test: /\.pug$/,
          loader: 'pug-loader',
          options: {
            pretty: true
          }
        }
      ]
    },
    plugins: [
      ...PUG_PAGE_FILES.map((page) => {
        const { dir, base, name } = path.parse(page);
        return new HtmlWebpackPlugin({
          template: `${dir}/${base}`,
          filename: `./${base.replace(/\.pug/, '.html')}`,
          chunks: ['vendor', 'common', name]
        });
      }).concat([
        new webpack.ProvidePlugin({
          $: 'jquery',
          jQuery: 'jquery'
        })
      ])
    ]
  };
};
