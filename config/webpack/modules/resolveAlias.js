const { PATHS } = require('../utils/constants');

module.exports = function () {
  return {
    alias: {
      assets: PATHS.assets,
      styles: PATHS.styles,
      images: PATHS.images,
      fonts: PATHS.fonts
    }
  };
};
