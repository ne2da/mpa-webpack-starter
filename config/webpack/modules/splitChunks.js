module.exports = () => {
  return {
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /node_modules/,
          name: 'vendor',
          chunks: 'all'
        },
        common: {
          chunks: 'all',
          name: 'common',
          minChunks: 2,
          minSize: 0
        }
      }
    }
  };
};

// optimization: {
//   splitChunks: {
//     chunks: 'all',
//     cacheGroups: {
//       commons: {
//         name: 'common',
//         chunks: 'initial',
//         minChunks: 1,
//       },
//     },
//   },
// },
