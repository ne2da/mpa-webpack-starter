const PROJECT_ROOT = require('app-root-path').path;
const path = require('path');
const { getFiles } = require('./utils');

// const ROOT = process.cwd();

const PATHS = {
  PROJECT_ROOT,
  build: path.resolve(PROJECT_ROOT, 'build'),
  src: path.resolve(PROJECT_ROOT, 'src'),
  pages: path.resolve(PROJECT_ROOT, 'src/pages'),
  components: path.resolve(PROJECT_ROOT, 'src/components'),
  scripts: path.resolve(PROJECT_ROOT, 'src/common/scripts'),
  styles: path.resolve(PROJECT_ROOT, 'src/common/styles'),
  templates: path.resolve(PROJECT_ROOT, 'src/common/templates'),
  assets: path.resolve(PROJECT_ROOT, 'src/assets'),
  images: path.resolve(PROJECT_ROOT, 'src/assets/images'),
  fonts: path.resolve(PROJECT_ROOT, 'src/assets/fonts'),
  media: path.resolve(PROJECT_ROOT, 'src/assets/media')
};

const JS_PAGE_FILES = getFiles(PATHS.pages, 'js');
// console.log('JS_PAGE_FILES>>> ', JS_PAGE_FILES);

const PUG_PAGE_FILES = getFiles(PATHS.pages, 'pug');
// console.log('PUG_PAGE_FILES >>> ', PUG_PAGE_FILES);

const SCSS_PAGE_FILES = getFiles(PATHS.pages, 'scss');
// console.log('SCSS_PAGE_FILES >>> ', SCSS_PAGE_FILES);

const ENTRIES = getEntries(JS_PAGE_FILES);

function getEntries(pathToEntries) {
  return pathToEntries.reduce((acc, current) => {
    const { dir, base, name, ext } = path.parse(current);
    return { ...acc, [name]: current }; // #1
    // return { ...acc, [name]: path.join(`./${name}`, base) }; // #2
    /* 
      Вариант записи #2 можно использовать, если в конфиге Webpack задать поле
      context: PATHS.pages, т.е. /home/neon/user-d/self-education/programming/web-dev/frontend/starter-kit/experimental/mpa-webpack-starter/src/pages/
      Тогда в таком случае, пути для точек входа задаются уже относительно
      контекста, указанного в поле context: PATHS.pages
      в виде entries: { about: './about/about.js', home: './home/home.js' }
  */
  }, {});
}

// console.log('ENTRIES >>> ', ENTRIES);
// console.log('Templates path >>> ', PATHS.templates);
// console.log('PATHS.PROJECT_ROOT >>> ', PATHS.PROJECT_ROOT);

const FILENAMES = {
  FILENAME_PROD: '[id].[name]-[hash:7].[ext]',
  FILENAME_DEV: '[name].[ext]'
};

module.exports.PATHS = PATHS;
module.exports.ENTRIES = ENTRIES;
module.exports.FILENAMES = FILENAMES;
module.exports.PUG_PAGE_FILES = PUG_PAGE_FILES;
