const fs = require('fs');

function getFiles(dir, ext) {
  let results = [];
  const list = fs.readdirSync(dir);
  list.forEach(function (file) {
    file = `${dir}/${file}`;
    const stat = fs.statSync(file);
    if (stat && stat.isDirectory()) {
      /* Recurse into a subdirectory */
      results = results.concat(getFiles(file, ext));
    } else {
      /* Is a file */
      const fileType = file.split('.').pop();
      const fileName = file.split(/(\\|\/)/g).pop();
      if (fileType === `${ext}`) results.push(file);
    }
  });
  return results;
}

module.exports = { getFiles };
