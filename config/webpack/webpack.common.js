// Webpack loaders
const webpack = require('webpack');
const merge = require('webpack-merge');
const loadFonts = require('./modules/loadFonts');
const resolveAlias = require('./modules/resolveAlias');
const splitChunks = require('./modules/splitChunks');
const loadPug = require('./modules/loadPug');
const babeLoader = require('./modules/babel');
const css = require('./modules/css');
const sass = require('./modules/sass');

// Core

// Tools | Helpers
const {
  PATHS,
  ENTRIES,
  FILENAMES: { FILENAME_DEV }
} = require('./utils/constants');

const common = {
  entry: ENTRIES,
  resolve: resolveAlias(),
  optimization: splitChunks(),
  output: {
    path: PATHS.build,
    filename: `js/${FILENAME_DEV}`,
    publicPath: '/'
  }
};

module.exports = () => {
  return merge([common, babeLoader(), loadPug(), css(), sass(), loadFonts()]);
};
