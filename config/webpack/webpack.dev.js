// Core
const merge = require('webpack-merge');
const getCommonConfig = require('./webpack.common');

const devServer = require('./modules/devserver');
const devtool = require('./modules/devtool');
const loadImages = require('./modules/loadImages');

module.exports = () => {
  return merge([getCommonConfig(), devServer(), devtool(), loadImages()]);
};
