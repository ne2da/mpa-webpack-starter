const merge = require('webpack-merge');

// Instruments
const getCommonConfig = require('./webpack.common');
const CleanWebpackPlugin = require('./modules/cleanWebpackPlugin');
const compressImages = require('./modules/loadImagesProd');

// Plugins

module.exports = () => {
  return merge([getCommonConfig(), CleanWebpackPlugin(), compressImages()]);
};
