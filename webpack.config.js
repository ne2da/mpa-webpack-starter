// Core
const merge = require('webpack-merge');
// Configs
const common = require('./config/webpack/webpack.common');
const dev = require('./config/webpack/webpack.dev');
const prod = require('./config/webpack/webpack.prod');

// module.exports = (env, argv) => {
//   if (argv.mode === 'development') {
//     return merge(common, dev);
//   }

//   if (argv.mode === 'production') {
//     return merge(common, prod);
//   }
// };

const IS_DEV_MODE = process.env.NODE_ENV === 'development';
const IS_PROD_MODE = !IS_DEV_MODE;

module.exports = () => {
  if (IS_DEV_MODE) {
    return merge([common(), dev()]);
  }

  if (IS_PROD_MODE) {
    return merge([common(), prod()]);
  }
};
